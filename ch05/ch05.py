def get_coach_data(fileName):
    try:
        with open(fileName) as f:
            data = f.readline()
            return data.strip().split(',')
    except IOError as err:
        print('File error : ' + str(err))
        return None


def sanitize(time_string):
    if '-' in time_string:
        splitter = '-'
    elif ':' in time_string:
        splitter = ':'
    else:
        return time_string
    (mins, secs) = time_string.split(splitter)
    return (mins + '.' + secs)


def sanitize_list(time_string_list):
    ret = []
    for each_item in time_string_list:
        ret.append(sanitize(each_item))
    return ret


# james = sanitize_list(readData(jamesf))
# julie = sanitize_list(readData(julief))
# mikey = sanitize_list(readData(mikeyf))
# sarah = sanitize_list(readData(sarahf))

clean_julie = sorted(set([sanitize(each_item) for each_item in get_coach_data('julie.txt')]))
clean_mikey = sorted(set([sanitize(each_item) for each_item in get_coach_data('mikey.txt')]))
clean_sarah = sorted(set([sanitize(each_item) for each_item in get_coach_data('sarah.txt')]))
clean_james = sorted(set([sanitize(each_item) for each_item in get_coach_data('james.txt')]))

# print(sorted(james))
# print(sorted(julie))
# print(sorted(mikey))
# print(sorted(sarah))
print(clean_james[0:3])
print(clean_julie[0:3])
print(clean_mikey[0:3])
print(clean_sarah[0:3])
