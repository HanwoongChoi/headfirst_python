movies = ['The Holy Grail', 1975, 'Terry Jones & Terry Gilliam', 91,
          ['Graham Chapan', ['Michael Palin', 'John Cleese', 'Terry Gilliam', 'Eric Idle', 'Terry Jones']]]

print(movies)
print('-' * 1000)

for each_item in movies:
    print(each_item)
print('-' * 1000)

for each_item in movies:
    if isinstance(each_item, list):
        for nested_item in each_item:
            print(nested_item)
    else:
        print(each_item)
print('-' * 1000)


def print_lol(the_list):
    for each_item in the_list:
        if isinstance(each_item, list):
            print_lol(each_item)
        else:
            print(each_item)


print_lol(movies)
print('-' * 1000)
