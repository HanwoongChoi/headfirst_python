import nester
import pickle

man = []
other = []

"""
    sketch.txt에서 읽어와서 man, other 채움
"""
try:
    with open('../ch03/sketch.txt') as data:
        for each_line in data:
            try:
                (role, line_spoken) = each_line.split(':', 1)
                line_spoken = line_spoken.strip()
                if role == 'Man':
                    man.append(line_spoken)
                elif role == 'Other Man':
                    other.append(line_spoken)
            except ValueError:
                pass
except IOError:
    print('The datafile is missing!')

try:
    with open('man_data.txt', 'w') as man_file, open('other_data.txt', 'w') as other_file:
        nester.print_lol(man, fh=man_file)
        nester.print_lol(other, fh=other_file)
        # print(man, file=man_file)
        # print(other, file=other_file)
except IOError:
    print('File error!')

try:
    with open('man_data.txt') as man_data:
        print(man_data.readline())
except IOError as err:
    print(str(err))

"""
    pickling을 통한 dump/load
"""
try:
    with open('man_data.txt', 'wb') as man_file, open('other_data.txt', 'wb') as other_file:
        pickle.dump(man, man_file)
        pickle.dump(other, other_file)
except IOError as err:
    print('File error : ', str(err))
except pickle.PickleError as perr:
    print('Pickling error: ', str(perr))

new_man = []
try:
    with open('man_data.txt', 'rb') as man_file:
        new_man = pickle.load(man_file)
except IOError as err:
    print('File error : ', str(err))
except pickle.PickleError as perr:
    print('Pickling error: ', str(perr))
nester.print_lol(new_man)