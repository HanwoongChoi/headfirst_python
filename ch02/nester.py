"""
주석 테스트1
"""
import sys


def print_lol(the_list, indent=False, level=0, fh=sys.stdout):
    """
    주석 테스트2
    :param level:
    :param the_list:
    :return:
    """
    for each_item in the_list:
        if isinstance(each_item, list):
            print_lol(each_item, indent, level + 1, fh)
        else:
            if indent:
                for tab_stop in range(level):
                    print('\t', end='', file=fh)
            print(each_item, file=fh)


# movies = ['The Holy Grail', 1975, 'Terry Jones & Terry Gilliam', 91,
#           ['Graham Chapan', ['Michael Palin', 'John Cleese', 'Terry Gilliam', 'Eric Idle', 'Terry Jones']]]
#
# print_lol(movies, 0)
# print('-' * 1000)
# print_lol(movies, True)
# print('-' * 1000)
# print_lol(movies, True, 2)
# print('-' * 1000)
