from distutils.core import setup

setup(
    name='nester',
    version='1.2.0',
    py_modules=['nester'],
    author='woong23',
    author_email='hwoong23@hotmail.com',
    url='http://www.headfirstlabs.com',
    description='A simple printer of nested lists',
)

"""
    make package: python setup.py sdist
    install package: python setup.py install

"""
